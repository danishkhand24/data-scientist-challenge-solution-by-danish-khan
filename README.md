# Data Science Challenge - Solution

This repository contains my solution to the ESProfiler data scientist challenge, where I have applied data science techniques to analyze the provided dataset and generate the required JSON file along with a visual representation.

## Problem Statement

The challenge was to analyze the Mitre Attack Stix dataset and parse it to create a json document with a single array containing technique objects, and then a sub array in each of these technique objects containing the adversaries who use this technique .

## Requirements

The following requirements need to be satisfied before executing the code:

- Python 3
- Matplotlib

## Dataset Analysis

An initial analysis of the dataset was conducted to understand the distributions of various features
Key findings from the analysis are summarized below:

- There are several attack patterns listed in the dataset
- Large number of malwares are listed
- Objects defining relationships between various entities

## Data Processing

The data was parsed to make it suitable for our objective. The steps included:

- Removing all the unnecessary details such as security solutions.
- Traversing over the techniques to find their relationships with adversaries.
- Creating a JSON object to store the matching objects and their adversaries.
- Calculating the frequency of each technique to generate the final graph

## Visualisation

As per requirement a graph representing the frequency of a technique was needed, however, the total number of attacks are a lot to be generated in a single graph. Therefore, I took the following approach to visualisig them:

- Sorted the dictionary based on the frequency of the techniques
- Filtered the top-10 mostly used techniques (this can be adjusted in the code as per needs)
- Generated a horizontal bar graph to visualize how often a technique was used

## Hardest and Easiest Segments

The overall task itself was of normal difficulty, however, identifying the identity of the object was not straight forward as the keywords used in the dataset are different from what we expected them to be which is quite normal.
After that point it was just a matter of parsing and organising the data to meet the requirements which did not take too long.

There could have been optimisation to increase the performance provided I had more time. Moreover, further detailed graphical illustrations could have also been created which can provide a deeper insight into the data.


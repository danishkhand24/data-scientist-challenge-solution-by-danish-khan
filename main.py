import json
import matplotlib.pyplot as plt

# loading the json file
file = open("enterprise-attack-14.0.json")
data = json.load(file)

main_obj = None
for obj in data["objects"]:
    if obj["type"] == "x-mitre-collection":
        main_obj = obj
        break

count = {}
adversaries = {}

# traversing and filtering all the objects which has a relationship type 'uses'
for references in main_obj["x_mitre_contents"]:
    for obj in data["objects"]:
        if references["object_ref"].startswith("attack-pattern"):
            if obj["type"] == "relationship" and obj["relationship_type"] == "uses" \
                    and (obj["target_ref"] == references["object_ref"]):
                if references["object_ref"] in adversaries:
                    adversaries[references["object_ref"]].append(obj["source_ref"])
                else:
                    adversaries[references["object_ref"]] = [obj["source_ref"]]

# calculating the frequency of each attack for graphical representation
json_obj = {"object": []}
for key, value in adversaries.items():
    json_obj["object"].append({"name": key, "used_by": value})
    count[key] = len(value)

# creating the json file
output = open("output.json", "w")
json.dump(json_obj, output)
output.close()

# sorting the dictionary to represent the top-10 techniques
list_to_sort = [(key, value) for key, value in count.items()]
list_to_sort.sort(key=lambda x: x[1], reverse=True)
list_to_sort = list_to_sort[:10]

xAxis = [key for (key, value) in list_to_sort]
yAxis = [value for (key, value) in list_to_sort]

# Horizontal Bar graph
fig = plt.figure(figsize=[30, 10])
plt.barh(xAxis, yAxis, color='maroon')
plt.xlabel('Frequency')
plt.ylabel('Technique')

plt.show()
